from django.contrib import admin
from django.shortcuts import redirect


from .models import *


class PollAdmin(admin.ModelAdmin):
	search_fields = ('name',)
	list_filter = ('date',)

class AlternativeInline(admin.TabularInline):
    model = Alternative
    fields = ("description",)

class QuestionAdmin(admin.ModelAdmin):
	search_fields = ('title',)
	list_filter = ('poll','kind')

	inlines = [
        AlternativeInline,
    ]

class AlternativeAdmin(admin.ModelAdmin):
	list_filter = ('question',)

class AlternativeSortInline(admin.TabularInline):
	model = AlternativeSort
	fields = ('alternative','order')
	readonly_fields=('alternative','order',)
	def has_delete_permission(self, request, obj=None):
		return False

class ResponseQuestionAdmin(admin.ModelAdmin):
	list_filter = ('question__poll','question')
	inlines = [
		AlternativeSortInline
	]
	readonly_fields=('question','text','alternative')
	def has_delete_permission(self, request, obj=None):
		return False

admin.site.register(Poll, PollAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Alternative, AlternativeAdmin)
admin.site.register(ResponseQuestion, ResponseQuestionAdmin)

