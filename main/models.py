from django.db import models



# Create your models here.

class Poll(models.Model):
	name = models.CharField("Name", max_length = 50)
	date = models.DateField("Date")
	description = models.CharField("Description", max_length = 100)
	def __str__(self):
		return self.name

class Question(models.Model):
	ALTERNATIVES = 1
	TEXT = 2
	SORT = 3
	OPTIONS = (
        (ALTERNATIVES, 'alternatives'),
        (TEXT, 'text'),
        (SORT, 'sort')
    	)
	poll = models.ForeignKey(Poll, on_delete=models.CASCADE, related_name="questions")
	kind = models.IntegerField("Type", choices=OPTIONS, default=TEXT)
	title = models.CharField("Title", max_length = 40)
	description = models.CharField("Description", max_length = 50)
	def __str__(self):
		return self.title

class Alternative(models.Model):
	question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name="alternatives")
	description = models.CharField("Description", max_length = 40)
	def __str__(self):
		return self.question.title + ": " +self.description

class ResponseQuestion(models.Model):
	question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name="responses")
	text = models.CharField("Text", max_length = 50, null = True, blank = True)
	alternative = models.ForeignKey(Alternative, on_delete=models.CASCADE, null = True)

class AlternativeSort(models.Model):
	responseQuestion = models.ForeignKey(ResponseQuestion, on_delete=models.CASCADE, related_name="responses")
	alternative = models.ForeignKey(Alternative)
	order = models.IntegerField("Order")


