from rest_framework import serializers
from .models import *

class PollSerializer(serializers.HyperlinkedModelSerializer):
	questions = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='question-detail'
    )

	class Meta:
		model = Poll
		fields = ('id','name','date','description','questions')

class AlternativeSerializer(serializers.ModelSerializer):
	class Meta:
		model = Alternative
		fields = ('id','description')

class QuestionSerializer(serializers.ModelSerializer):
	alternatives = AlternativeSerializer(many=True, read_only=True)
	kind =  serializers.SerializerMethodField('is_kind')
	def is_kind(self, item):
		switcher = {
			1:"alternatives",
			2:"text",
			3:"sort"
		}
		return switcher.get(item.kind, "invalid")
	class Meta:
		model = Question
		fields = ('id', 'title','description','kind','alternatives')

class QuestionReportSerializer(serializers.ModelSerializer):
	class Meta:
		model = Question
		fields = ('title','description')

class ResponseQuestionSerializer(serializers.ModelSerializer):
	order  = serializers.CharField(max_length = 10, required = False)
	class Meta:
		model = ResponseQuestion
		fields = '__all__'

	def create(self, validated_data):
		try:
			validated_data.pop("order")
		except:
			pass
		return ResponseQuestion.objects.create(**validated_data)

class OrderStatisticsSerializer(serializers.Serializer):
	order = serializers.IntegerField()
	percentage = serializers.IntegerField()

class StatisticsSerializer(serializers.Serializer):
	text = serializers.CharField(max_length = 100, required = False)
	alternative = serializers.CharField(max_length = 40, required = False) 
	percentage = serializers.CharField(max_length = 5, required = False)
	total = serializers.IntegerField(required = False)
	orders =  OrderStatisticsSerializer(many = True, required = False)

class ReportSerializer(serializers.Serializer):
	question = QuestionReportSerializer()
	statistics = StatisticsSerializer(many = True)
