from django.conf.urls import url
from .views import *

from rest_framework import routers

router = routers.SimpleRouter()
router.register(r'response', ResponseQuestionAPIView)
router.register(r'polls', PollAPIView)
router.register(r'questions', QuestionAPIView)
router.register(r'report', ReportViewSet, basename="reports")

urlpatterns = router.urls


urlpatterns += [
]