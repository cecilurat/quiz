from django.shortcuts import render
from rest_framework import generics
from rest_framework.views import APIView
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.response import Response
from rest_framework import viewsets
from django.db.models import Q, Count
from decimal import Decimal
from collections import Counter 

from .models import *
from .serializer import * 


class PollAPIView(viewsets.ReadOnlyModelViewSet):
	queryset = Poll.objects.all()
	serializer_class = PollSerializer


class QuestionAPIView(viewsets.ReadOnlyModelViewSet):
	queryset = Question.objects.all()
	serializer_class = QuestionSerializer
	
class ReportViewSet(viewsets.ViewSet):
	def AlternativeSerializer(self, pk):
		question = Question.objects.get(pk = pk)
		question_serializer = QuestionReportSerializer(question)
		_responses = ResponseQuestion.objects.filter(question = pk).exclude(alternative = None)
		responses = _responses.values_list('alternative', flat=True).distinct()
		details = []
		for r in responses:
			alternative = Alternative.objects.get(pk = r).description
			total = len(_responses)
			count = len(_responses.filter(alternative = r))
			detail = StatisticsSerializer(data={
				"alternative": alternative,
				"percentage": round(Decimal((count/total)*100), 2),
				"total": count
			})	
		
			detail.is_valid()
			details.append(detail.data)
		serializer = ReportSerializer(data={
			"question": question_serializer.data,
			"statistics": details
			})
		serializer.is_valid()
		return serializer.data

	def TextSerializer(self, pk):
		question = Question.objects.get(pk = pk)
		question_serializer = QuestionReportSerializer(question)
		responses = ResponseQuestion.objects.filter(question = pk).exclude(text = None).values("text")
		text = ""
		for r in responses:
			text = text + " " + r["text"]

		text_it = text.split()
		counter = Counter(text_it)
		most_occur = counter.most_common(50)
		count = 0
		words = ""
		for m in most_occur:
			if count == 0:
				words = m[0] + ":" + str(round(Decimal((m[1]/len(text_it))*100), 2)) + "%"
				count = count + 1
			else:
				words = words + " " + m[0] + ":" + str(round(Decimal((m[1]/len(text_it))*100), 2)) + "%"

		details = []
		detail = StatisticsSerializer(data={
			"text": words,
			})
		detail.is_valid()
		details.append(detail.data)
		serializer = ReportSerializer(data={
			"question": question_serializer.data,
			"statistics": details
			})
		serializer.is_valid()
		return serializer.data

	def AlternativeSortSerializer(self, pk):
		question = Question.objects.get(pk = pk)
		question_serializer = QuestionReportSerializer(question)
		responses = ResponseQuestion.objects.filter(question = pk).values("pk")
		_alternatives = AlternativeSort.objects.filter(responseQuestion__in = responses)
		alternatives = _alternatives.values_list("alternative").distinct()
		details = []
		for a in alternatives:
			altern = Alternative.objects.get(pk = a[0]).description
			_alternative = _alternatives.filter(alternative = a[0])
			alternative = _alternative.values_list("order").distinct()
			orders = []
			total = len(_alternative)
			for al in alternative:
				count = len(_alternative.filter(order = al[0]))
				order = OrderStatisticsSerializer(data={
					"order": al,
					"percentage": round(Decimal((count/total)*100), 2)
					})
				order.is_valid()
				orders.append(order.data)
			
			detail = StatisticsSerializer(data={
				"alternative": altern,
				"orders": orders
				})
			detail.is_valid()
			details.append(detail.data)
		serializer = ReportSerializer(data={
			"question": question_serializer.data,
			"statistics": details
			})
		serializer.is_valid()
		return serializer.data

	def switcher_case(self, kind, pk):
		switcher = {
			1: self.AlternativeSerializer(pk),
			2: self.TextSerializer(pk),
			3: self.AlternativeSortSerializer(pk)
		}
		return switcher.get(kind)
	def retrieve(self, request, pk=None):
		questions = Question.objects.filter(poll = pk)
		serializer = []
		for q in questions:
			serializer.append(self.switcher_case(q.kind, q.id))
		return Response(serializer)



class ResponseQuestionAPIView(viewsets.ModelViewSet):
	queryset = ResponseQuestion.objects.all()
	serializer_class = ResponseQuestionSerializer
	def perform_create(self, serializer):
		question = Question.objects.get(pk = self.request.data.get("question"))
		_orders = self.request.data.get("order")
		obj = serializer.save()
		if question.kind == 3:
			orders = _orders.split(",")
			count = 1
			for o in orders:
				alternative = Alternative.objects.get(pk = o)
				sort = AlternativeSort(
					responseQuestion = obj,
					alternative = alternative,
					order = count,
				)
				sort.save()
				count = count + 1
		