**Desarrollo de Encuestas dinamicas**

Las encuestas son gestionadas desde el administrador de Django y la informacion de encuestas es recogida mediante endpoints. Tambien se puede visualizar los reportes de los resultados. El proyecto para pruebas se encuentra alojado en: http://52.41.46.242:8080/admin/

---

## Instrucciones y requerimientos

Instrucciones:

1. Creacion del entorno virtual y posterior activación
2. Clonar el desarrollo dentro del entorno
3. Instalar las librerias dependientes (Requerimientos).
4. Correr el desarrollo.

Requerimientos (librerias):

1. Django==1.11
2. django-cors-headers==2.5.0 (para que pueda ser consumida por un Cliente REST)
3. django-filter==2.2.0
4. djangorestframework==3.9.2
5. djangorestframework-jwt==1.11.0

Posibles problemas:

1. Por la versiòn de Django al momento de ingresar al Administrador y luego observar los endpoints, puede surgir este error: Django 'CSRFCheck' object has no attribute 'process_request', se encontro soluciòn en el siguiente enlace:https://stackoverflow.com/questions/54609495/django-csrfcheck-object-has-no-attribute-process-request.



---

## Administrador

Para ingresar al administrador estos son los accesos. 
Usuario: admin 
Contraseña: quiz!2019

1. En el modulo MAIN se encuentra POLL (Encuestas) donde se pueden gestionar las encuestas y realizar busqueda por fecha.
2. En el mismo modulo se encuentra QUESTION (Preguntas) para su gestion con el friltro de encuenta y tipo de pregunta.
3. Tambien en MAIN estan ALTERNATIVES (Alternativas) para ser gestionadas con filtro de preguntas.
4. Como informacion solo visuable en MAIN esta RESPONSE QUESTION (respuestas) se puede visualizar informacion de cada tipo y se puede filtrar por encuesta y pregunta.


Se puede visualizar el administrador en el siguiente enlace: http://52.41.46.242:8080/admin/

---

## Endpoints de encuestas y preguntas

En los siguientes enpoints solo se puede visualizar la informacion:

1. Lista de encuestas con enlaces a sus preguntas: http://52.41.46.242:8080/api/main/polls/
2. Informacion de cada preguntas y alternativas (si las tuviera) http://52.41.46.242:8080/api/main/questions/(pk)/

---

## Endpoint de respuestas

El endpoint para ingresar una respuesta es unico y puede ser dinamico por el tipo de pregunta: http://52.41.46.242:8080/api/main/response/

1. Para preguntas de texto, enviar: "question" (id de la pregunta), "text" (respuesta de la pregunta)
2. Para preguntas de alternativa, enviar: "question" (id de la pregunta), "alternative" (id de la alternativa escogida)
3. Para preguntas de ordenamiento, enviar: "question": (id de la pregunta), "order" (los id de las alternativas con el orden escogido, separado por "," comas)


---

## Endpoint de reportes

El endpoint para reportes es unico pero es dinamico para el tipo de encuesta: http://52.41.46.242:8080/api/main/report/(poll-id)/
Se visualizan todas las preguntas de la encuesta con las estadisticas en cada una por su tipo. 
Ejemplo: http://52.41.46.242:8080/api/main/report/1/

1. Para el reporte de tipo alternativa: se visualiza la alternativa, el porcentaje, el total de veces que ha sido escogida esa alternativa.
2. Para el reporte de tipo texto: en el campo texto se encuentra la lista de las 50 palabras mas usadas con el formato (palabra:porcentaje)
3. Para el reporte de tipo ordenamiento: se enlista las alternativas, en cada alternativa se visualiza los ordenes en el que ha sido asignada y el porcentaje que representa ese orden del total.






